var express = require('express');
var router = express.Router();
var csv = require("fast-csv");
var db = require('../config/db');
var Lead = require('../models/leads');
var fs = require('fs');


var fs = require('fs');

router.get('/UploadFile/:file', function(req, res){
	var path = "./upload/" + req.params.file +".csv";

	fs.exists(path, function(exists) {
	    if (exists) {
	        

	    	var stream = fs.createReadStream(path);
 
			csv.fromStream(stream, {headers : [
			 								"Title",
			 								"Forename",
			 								"Surname",
			 								"Company Name",
			 								"Address Line 1",
			 								"Address Line 2",
			 								"Address Line 3",
			 								"Town",
			 								"County",
			 								"Post Code",
			 								"Home Telephone Number",
			 								"Work Telephone Number",
			 								"Mobile Telephone",
			 								"Email Address"]})
			 .on("data", function(data){
			 	var newLead = new Lead();
			 	newLead.company = data['Company Name'];
			 	newLead.address = [{
			 		addr1: data['Address Line 1'],
			 		addr2: data['Address Line 2'],
			 		addr3: data['Address Line 3'],
			 		addrtown: data.Town, 
					addrcity: "",
					addrcounty: data.County,
					addrpostcode: data['Post Code']
			 	}]
			 	newLead.contacts= 
					[
						{
							salutation: data.Title,
							firstname: data.Forename,
							surname: data.Surname,
							title: "",
							homeTelephone: data['Home Telephone Number'],
							workTelephone: data['Work Telephone Number'],
							mobileTelephone: data['Mobile Telephone'],
							email: data['Email Address']
						}
					]
				//console.log(newLead);
				newLead.save(function (err, data) {
					if (err) console.log(err);
					else {
						//console.log('Saved ', data );
					}
				});
			 	/*db.leads.insert(newLead, function(err, doc){
			  		//res.json(doc);
			  	});*/
			    //console.log(data);
			 })			
			 .on("end", function(){
			    console.log("done");
			    var objToday = new Date();
				var dd = objToday.getDate();
				var mm = objToday.getMonth()+1; //January is 0!
				var yyyy = objToday.getFullYear();
				var curHour = objToday.getHours();
				var curMinute = objToday.getMinutes() ;
				var curSeconds = objToday.getSeconds() ;
				var pathold =  "./upload/" + req.params.file +".csv";
				var pathnew =  "./upload/archive/" + req.params.file + "_" + yyyy + mm + dd + curMinute + curHour +".csv";
				

				console.log(pathold);
				console.log(pathnew);
				fs.rename(pathold, pathnew, function(err) {
					console.log('rename callback ', err);
				})
			    res.render('index',{ title: 'Upload Complete' });
			 });



	    }
	    else
	    {
	    	console.log('File to upload Not Found: ' + path)
	    	res.render('index',{ title: 'File ' + req.params.file + ' not found' });
	    }
	});
	
	
});

module.exports = router;

