var express = require('express');
var router = express.Router();
var users = require('./users');
var leads = require('./leads');
var version_control = require('./version_control')
var uploadCSV = require('./uploadCSV');
/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Welcome to ROI GPT' });
});
router.use('/api/users', users);
router.use('/api/leads', leads);
router.use('/api/versioncontrol', version_control);
router.use('/uploadCSV', uploadCSV);

router.get('*', function(req, res){
  //res.status(404).send('what???');
  res.render('404')
});

module.exports = router;
