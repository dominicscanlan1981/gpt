var express = require('express');
var router = express.Router();

var Lead = require('../models/leads');
var VersionControl = require('../models/version_control');

router.get('/', function(req, res){
	Lead.find({}).exec(function(err, leads){
		res.send(leads);
	});
});

router.get('/:id', function(req, res){
	Lead.findById(req.params.id).exec(function(err, lead){
		res.send(lead);
	});

	
});

router.get('/findByName/:name', function(req, res){
	Lead.find({ company: req.params.name}).exec(function(err, lead){
		res.send(lead);
	});

	
});

router.get('/findByContactName/:surname', function(req, res){
	Lead.find({"contacts.surname":req.params.surname}).exec(function(err, leads){
		res.send(leads);
	});
});

router.post('/', function(req, res) {
	var newLead = new Lead();
	 	newLead.company = req.data.body.company;
	 	newLead.address = req.data.body.address;
	 	newLead.contacts= req.data.body.contacts;
		console.log(newLead);
		newLead.save(function (err, data) {
			if (err) console.log(err);
			else console.log('Saved ', data );
		});

});

router.put('/:id', function(req, res) {
	// we're updating so add the original to version_control
	Lead.findById(req.params.id).exec(function(err, lead){
		
	
		var version = new VersionControl();
		version.previous= lead;
		version.new= req.body;
		version.date= new Date();
		version. updatedBy= req.body.userid;
		
		version.save(function (err, data) {
			if (err){ console.log(err);}
			else {
				console.log('Saved ', data );
				Lead.findAndModify({
			    query: { _id: mongojs.ObjectId(req.params.id) },
			    update: { company: req.body.company }
			});
			}
		});

		VersionControl.insert(version, function(err, vc){
			
		})

	  	
	});
});

/*router.delete('/', function(req, res) {
  res.send('deleted.');
});*/

module.exports= router;