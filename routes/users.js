var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('respond with a resource');
});

router.post('/', function(req, res) {
  res.send('posted.');
});

router.put('/', function(req, res) {
  res.send('Put.');
});

router.delete('/', function(req, res) {
  res.send('deleted.');
});



module.exports = router;
