var express = require('express');
var router = express.Router();

var VersionControl = require('../models/version_control');

router.get('/', function(req, res){
	VersionControl.find({}).exec(function(err, vcs){
		res.send(vcs);
	});
	
});

router.get('/:id', function(req, res){
	VersionControl.findById(req.params.id).exec(function(err, vc){
		res.send(vc);
	});

	
});


router.get('/findCompanyName/:compname', function(req, res){
	VersionControl.find({"previous.company":req.params.compname}).exec(function(err, vcs){
		res.send(vcs);
	});
	
});

router.get('/UpdatedByUser/:userid', function(req, res){
	VersionControl.find({"updatedBy":req.params.userid}).exec(function(err, vcs){
		res.send(vcs);
	});
	
});


module.exports = router;