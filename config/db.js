//////////////// DATABASE
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/gpt');
var db = mongoose.connection;

db.on('error', function(msg){
    console.log("db connection failed");
});

db.once('open', function(){
    console.log("db connected successfully");
});

/*fs.readdirSync(__dirname + '/models').forEach(function(filename){
    if(~filename.indexOf('.js')){
        require(__dirname + '/models/' + filename);
    }
})*/

//////////////// DATABASE END