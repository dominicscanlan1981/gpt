var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var versionControlSchema = new Schema({
	previous: {
		_id: Schema.Types.ObjectId,
		company: String, 
		address:
		[
			{
			addr1: String,
			addr2: String,
			addr3: String,
			addr4: String,
			addrtown: String, 
			addrcity: String,
			addrcounty: String,
			addrpostcode: String
			}
		],
		contacts:
		[
			{
				salutation: String,
				firstname: String,
				surname: String,
				title: String,
				hometelephone: String,
				workTelephone: String,
				mobileTelephone: String,
				email: String
			}
		]
	},
	new: {
		_id: Schema.Types.ObjectId,
		company: String, 
		address:
		[
			{
			addr1: String,
			addr2: String,
			addr3: String,
			addr4: String,
			addrtown: String, 
			addrcity: String,
			addrcounty: String,
			addrpostcode: String
			}
		],
		contacts:
		[
			{
				salutation: String,
				firstname: String,
				surname: String,
				title: String,
				homeTelephone: String,
				workTelephone: String,
				mobileTelephone: String,
				email: String
			}
		]
	},
	date: { type: Date, default: Date.now },
	updatedBy: Number

});

var VersionControl = mongoose.model('version_control', versionControlSchema);

module.exports = VersionControl;