var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var leadsSchema = new Schema({
	company: String, 
	address:
	[
		{
		addr1: String,
		addr2: String,
		addr3: String,
		addr4: String,
		addrtown: String, 
		addrcity: String,
		addrcounty: String,
		addrpostcode: String
		}
	],
	contacts:
	[
		{
			salutation: String,
			firstname: String,
			surname: String,
			title: String,
			homeTelephone: String,
			workTelephone: String,
			mobileTelephone: String,
			email: String
		}
	]

});

var Lead = mongoose.model('leads', leadsSchema);

module.exports = Lead;